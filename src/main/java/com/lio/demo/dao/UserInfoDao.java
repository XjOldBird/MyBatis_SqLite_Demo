package com.lio.demo.dao;

import com.lio.demo.model.UserInfoModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface UserInfoDao {

    @SelectProvider(type = UserInfoMapper.class,method = "findUserInfoList")
    List<UserInfoModel> findUserInfoList(String id);

}
