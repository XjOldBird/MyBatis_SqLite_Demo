package com.lio.demo.dao;

import com.alibaba.druid.util.StringUtils;
import org.apache.ibatis.jdbc.SQL;


public class UserInfoMapper {

    public String findUserInfoList(String id){
        SQL sql = new SQL();
        sql.SELECT("id,name,age,sex");
        sql.FROM("user_info");
        if(!StringUtils.isEmpty(id)){
            sql.WHERE("id=#{id}");
        }
        return sql.toString();
    }
}
