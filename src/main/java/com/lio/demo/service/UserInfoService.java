package com.lio.demo.service;

import com.lio.demo.dao.UserInfoDao;
import com.lio.demo.model.UserInfoModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserInfoService {

    @Autowired
    private UserInfoDao userInfoDao;

    public List<UserInfoModel> findUserInfoList(String id){
        return userInfoDao.findUserInfoList(id);
    }
}
