package com.lio.demo.model;

import lombok.Data;

@Data
public class UserInfoModel {
    private Integer id;
    private String name;
    private Integer age;
    private String sex;
}
