工具：

IntelliJ IDEA 2018.2 x64

Navicat Premium 12

涉及：

SpringBoot

SqLite

MyBatis

Swagger2

Lombok
<hr>

1、使用idea创建SpringBoot项目，添加web和MyBatis支持。
![New Project](http://upload-images.jianshu.io/upload_images/13440983-8f418d2c61c28888?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
2、修改pom.xml文件。添加swagger、lombok、SQLite驱动支持。
```
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.lio</groupId>
    <artifactId>demo</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>jar</packaging>

    <name>demo</name>
    <description>Demo project for Spring Boot</description>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.0.4.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>1.8</java.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>1.3.2</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>

        <!-- swagger 驱动 -->
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger-ui</artifactId>
            <version>2.7.0</version>
        </dependency>
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger2</artifactId>
            <version>2.7.0</version>
        </dependency>

        <!-- druid 驱动 -->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid</artifactId>
            <version>1.1.9</version>
        </dependency>

        <!-- lombok 驱动 -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.2</version>
        </dependency>

        <!-- SQLite 驱动 -->
        <dependency>
            <groupId>org.xerial</groupId>
            <artifactId>sqlite-jdbc</artifactId>
            <version>3.21.0.1</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>


</project>
```
3、利用IDEA快速添加SqLite，点击IDEA右侧的 `Database`  >>  `+`  >>  `Data Source`  >>  `Sqlite` 。
![Database](https://upload-images.jianshu.io/upload_images/13440983-c0e671ff2709bf5c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

4、未添加过Sqlite驱动的需要添加驱动，点击 `Sqlite（Xerial）` >>  `+`  >> `provided Drive`  >>  `Xerial SQLiteJDBC`  >>  `latest version`。
![Add Sqlite Driver](https://upload-images.jianshu.io/upload_images/13440983-d8f7fb3c177bedd9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

5、创建dome.db数据文件。
`Name：数据源名称，随意填写。`
`File：指定数据文件，点击 + 号可创建文件，如果存在db文件则点击 ... 选择文件路径。`
`Test Connection：点击测试连接是否正常。`
![Create dome.db](http://upload-images.jianshu.io/upload_images/13440983-b7ec1f3df32831e5?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

6、用Navicat打开dome.db，创建user_info表。打开Navicat 将创建好的dome.db直接拖入Navicat即可，建表不做说明。
![dome.db](http://upload-images.jianshu.io/upload_images/13440983-98794837c05773a5?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

7、为IDEA添加Lombok插件。
`File`  >>  `Settings`  >>  `Plugins`  >>  `输入lombok`  >>  `点击安装`
![add Lombok Plugin](http://upload-images.jianshu.io/upload_images/13440983-74c2d046bdfa9caa?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

8、创建SwaggerConfig.java配置文件。
```
package com.lio.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket createDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.lio.demo.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    public ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("MyBatis + SqLite 测试程序")
                .termsOfServiceUrl("NO terms of service")
                .version("1.0")
                .build();
    }
}
```
9、修改启动类，添加`@EnableSwagger2`注解，启用Swagger2。
```
package com.lio.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
```
10、创建UserInfo的实体类，这里使用到Lombok的@Data注解。
```
package com.lio.demo.model;

import lombok.Data;

@Data
public class UserInfoModel {
    private Integer id;
    private String name;
    private Integer age;
    private String sex;
}
```
11、创建UserInfoDao，添加@Mapper注解，声明Mapper。
```
package com.lio.demo.dao;

import com.lio.demo.model.UserInfoModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface UserInfoDao {

    @SelectProvider(type = UserInfoMapper.class,method = "findUserInfoList")
    List<UserInfoModel> findUserInfoList(String id);

}
```
12、创建UserInfoMapper。
```
package com.lio.demo.dao;

import com.alibaba.druid.util.StringUtils;
import org.apache.ibatis.jdbc.SQL;


public class UserInfoMapper {

    public String findUserInfoList(String id){
        SQL sql = new SQL();
        sql.SELECT("id,name,age,sex");
        sql.FROM("user_info");
        if(!StringUtils.isEmpty(id)){
            sql.WHERE("id=#{id}");
        }
        return sql.toString();
    }
}
```
13、创建UserInfoService。
```
package com.lio.demo.service;

import com.lio.demo.dao.UserInfoDao;
import com.lio.demo.model.UserInfoModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserInfoService {

    @Autowired
    private UserInfoDao userInfoDao;

    public List<UserInfoModel> findUserInfoList(String id){
        return userInfoDao.findUserInfoList(id);
    }
}
```
14、创建UserInfoController。
```
package com.lio.demo.controller;

import com.lio.demo.model.UserInfoModel;
import com.lio.demo.service.UserInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/user")
@Api(value = "用户类" ,tags = "用户操作接口")
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    @GetMapping(value = "/findUserInfoList")
    @ApiOperation(value="获取用户列表", notes="获取用户列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户ID", required = false, paramType = "query")
    })
    public List<UserInfoModel> findUserInfoList(String id){
        return userInfoService.findUserInfoList(id);
    }
}
```
15、至此代码已经写完了，启动程序进入Swagger测试。
Swagger地址：http://localhost:8080/swagger-ui.html
![Swagger](https://upload-images.jianshu.io/upload_images/13440983-54a7cbc9f615589b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)



参考
>[mybatis官方文档](http://www.mybatis.org/mybatis-3/zh/index.html)

>[swagger2常用注解说明](https://blog.csdn.net/u014231523/article/details/76522486)

>[Lombok介绍](https://blog.csdn.net/motui/article/details/79012846)